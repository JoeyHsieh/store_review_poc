//
//  StoreReviewPOCApp.swift
//  StoreReviewPOC
//
//  Created by Joey Hsieh on 2023/3/31.
//

import SwiftUI

@main
struct StoreReviewPOCApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
