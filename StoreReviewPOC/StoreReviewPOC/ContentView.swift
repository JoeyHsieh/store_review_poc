//
//  ContentView.swift
//  StoreReviewPOC
//
//  Created by Joey Hsieh on 2023/3/31.
//

import StoreKit
import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack(spacing: 32.0) {
            Button("iOS 14 or later", action: {
                onClick14Button()
            })
            Button("iOS 16 or later", action: {
                onClick16Button()
            })
            Button("Manually", action: {
                requestReviewManually()
            })
        }
        .padding()
    }
    
    func onClick14Button() {
        if let scene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }) as? UIWindowScene {
            DispatchQueue.main.async {
                SKStoreReviewController.requestReview(in: scene)
            }
            
        }
    }
    
    @Environment(\.requestReview) private var requestReview
    
    func onClick16Button() {
        DispatchQueue.main.async {
            requestReview()
        }
    }
    
    ///  Replace the XXX below with the App Store ID for your app
    func requestReviewManually() {
        guard let writeReviewURL = URL(string: "https://apps.apple.com/app/idXXX?action=write-review")
                else { fatalError("Expected a valid URL") }
            UIApplication.shared.open(writeReviewURL, options: [:], completionHandler: nil)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
